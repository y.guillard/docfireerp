from .base import *  # noqa @UnusedWildImport

DEBUG = True

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.postgresql',
        'NAME': 'doc-fireerp',
        'USER': 'davidherer',
        'PASSWORD': '',
        'HOST': 'localhost',
        'PORT': '',
    }
}

for template_engine in TEMPLATES:
    template_engine["OPTIONS"]["debug"] = True


EMAIL_BACKEND = "django.core.mail.backends.console.EmailBackend"


try:
    import debug_toolbar  # @UnusedImport

    MIDDLEWARE = list(MIDDLEWARE) + [
        "debug_toolbar.middleware.DebugToolbarMiddleware",
    ]
    INSTALLED_APPS = list(INSTALLED_APPS) + ["debug_toolbar"]
    INTERNAL_IPS = ("127.0.0.1",)
    DEBUG_TOOLBAR_CONFIG = {"INTERCEPT_REDIRECTS": False}
except ImportError:
    pass

from storages.backends.s3boto3 import S3Boto3Storage


S3_BUCKET = 'fe-doc'
S3_REGION = 'eu-west-3'
AWS_STORAGE_BUCKET_NAME = S3_BUCKET
AWS_S3_REGION_NAME = S3_REGION
AWS_ACCESS_KEY_ID = ''
AWS_SECRET_ACCESS_KEY = ''

AWS_QUERYSTRING_EXPIRE = 30
AWS_QUERYSTRING_AUTH = False
AWS_S3_SIGNATURE_VERSION = 's3v4'
AWS_DEFAULT_ACL = 'public-read'

class DeveloppementStorage(S3Boto3Storage):
    location = 'dev'

WIKI_IMAGE_PATH = 'images/%aid'
DEFAULT_FILE_STORAGE = 'testproject.settings.dev.DeveloppementStorage'
