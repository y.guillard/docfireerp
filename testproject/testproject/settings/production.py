from .base import *  # noqa @UnusedWildImport

DEBUG = False


S3_BUCKET = 'fe-doc'
S3_REGION = 'eu-west-3'
AWS_STORAGE_BUCKET_NAME = os.getenv('S3_BUCKET')
AWS_S3_REGION_NAME = os.getenv('S3_REGION')
AWS_ACCESS_KEY_ID = os.getenv('AWS_ACCESS_KEY_ID')
AWS_SECRET_ACCESS_KEY = os.getenv('AWS_SECRET_ACCESS_KEY')

AWS_QUERYSTRING_EXPIRE = 30
AWS_QUERYSTRING_AUTH = False
AWS_S3_SIGNATURE_VERSION = 's3v4'
AWS_DEFAULT_ACL = 'public-read'

WIKI_IMAGE_PATH = 'images/%aid'
DEFAULT_FILE_STORAGE = 'storages.backends.s3boto3.S3Boto3Storage'

import django_heroku
django_heroku.settings(locals())
